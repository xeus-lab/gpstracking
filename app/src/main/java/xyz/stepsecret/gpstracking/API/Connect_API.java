package xyz.stepsecret.gpstracking.API;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import xyz.stepsecret.gpstracking.Model.Connect_Model;

/**
 * Created by Assanee on 8/7/2558.
 */



public interface Connect_API {

    @GET("checkconnect")
    Call<Connect_Model> gerCheckConnect();


}
