package xyz.stepsecret.gpstracking.API;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import xyz.stepsecret.gpstracking.Model.Login_Model;

/**
 * Created by Assanee on 8/7/2558.
 */



public interface Login_API {


    @FormUrlEncoded
    @POST("login")
    Call<Login_Model> LoginAPI(@Field("username") String username, @Field("password") String password);


}
