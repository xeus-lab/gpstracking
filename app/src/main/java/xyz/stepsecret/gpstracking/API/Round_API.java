package xyz.stepsecret.gpstracking.API;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import xyz.stepsecret.gpstracking.Model.Round_Model;

/**
 * Created by Assanee on 8/7/2558.
 */



public interface Round_API {


    @GET("newrounds")
    Call<Round_Model> gerRound(@Query("api_key") String api_key);


}
