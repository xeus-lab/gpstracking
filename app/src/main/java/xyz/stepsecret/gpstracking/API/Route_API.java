package xyz.stepsecret.gpstracking.API;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import xyz.stepsecret.gpstracking.Model.Round_Model;
import xyz.stepsecret.gpstracking.Model.Route_Model;

/**
 * Created by Assanee on 8/7/2558.
 */

public interface Route_API {

    @GET("route")
    Call<Route_Model> gerRoute();


}
