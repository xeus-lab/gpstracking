package xyz.stepsecret.gpstracking.API;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import xyz.stepsecret.gpstracking.Model.TrackSave_Model;

/**
 * Created by Assanee on 8/7/2558.
 */



public interface TrackSave_API {


    @FormUrlEncoded
    @POST("tracksave")
    Call<TrackSave_Model> TrackSave(
            @Field("api_key") String api_key,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude,
            @Field("rounds") String rounds,
            @Field("velocity") String velocity,
            @Field("degree") int degree,
            @Field("status") String status,
            @Field("state") int state
        );

}
