package xyz.stepsecret.gpstracking;

/**
 * Created by Aiii_ on 7/10/2559.
 */

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Chronometer;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.securepreferences.SecurePreferences;

import ioio.lib.api.DigitalOutput;
import ioio.lib.api.IOIO;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.stepsecret.gpstracking.API.TrackSave_API;
import xyz.stepsecret.gpstracking.Config.ApiClient;
import xyz.stepsecret.gpstracking.Config.ConfigData;
import xyz.stepsecret.gpstracking.Model.TrackSave_Model;

public class BoundService extends IOIOService implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private Location mLastLocation;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    //private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    //private static int DISPLACEMENT = 10; // 10 meters

    private Location last_location;

    private int mAzimuth = 0; // degree

    private SensorManager mSensorManager = null;

    private Sensor mAccelerometer;
    private Sensor mMagnetometer;

    boolean haveAccelerometer = false;
    boolean haveMagnetometer = false;

    private Boolean checkpause;
    private Boolean checkround;
    private Boolean checkstart;
    private int round;

    private static String LOG_TAG = "BoundService";
    private IBinder mBinder = new MyBinder();

    private String route_name;
    private String start_name;
    private String target_name;
    private Double start_lat;
    private Double start_lng;
    private Double target_lat;
    private Double target_lng;
    private Double route_distance;

    private int State = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(LOG_TAG, "in onCreate");

        Log.e(" MyService "," Service onCreate");

        // initialize your android device sensor capabilities
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        haveAccelerometer = mSensorManager.registerListener(mSensorEventListener, this.mAccelerometer, SensorManager.SENSOR_DELAY_GAME);

        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        haveMagnetometer = mSensorManager.registerListener(mSensorEventListener, this.mMagnetometer, SensorManager.SENSOR_DELAY_GAME);

        if (haveAccelerometer && haveMagnetometer) {
            // ready to go
        } else {
            // unregister and stop
        }


        Initial();


        buildGoogleApiClient();

        createLocationRequest();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.e(LOG_TAG, "in onBind");
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        Log.e(LOG_TAG, "in onRebind");
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.e(LOG_TAG, "in onUnbind");
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(LOG_TAG, "in onDestroy");
        stopLocationUpdates();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    protected IOIOLooper createIOIOLooper() {
        return new BaseIOIOLooper() {
            private DigitalOutput led_;
            private DigitalOutput led_22;

            @Override
            protected void setup() throws ConnectionLostException,
                    InterruptedException {
                Log.e(" MyService "," setup");
                led_ = ioio_.openDigitalOutput(IOIO.LED_PIN);
                led_22 = ioio_.openDigitalOutput(22, true);

                showVersions(ioio_, "Incompatible firmware version!");
            }
            @Override
            public void disconnected() {

                showDisconnect();

            }


            @Override
            public void incompatible() {


            }


            @Override
            public void loop() throws ConnectionLostException,
                    InterruptedException {
                led_.write(false);
                led_22.write(false);
                Thread.sleep(500);
                led_.write(true);
                led_22.write(true);
                Thread.sleep(500);
                //showLoop();
                //Log.e(" MyService "," loop" +"");
            }
        };
    }

    private void showVersions(IOIO ioio, String status) {

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("ioio_connect");
        broadcastIntent.putExtra("Status", status);
        broadcastIntent.putExtra("IOIOLib", ioio.getImplVersion(IOIO.VersionType.IOIOLIB_VER));
        broadcastIntent.putExtra("Application", ioio.getImplVersion(IOIO.VersionType.APP_FIRMWARE_VER));
        broadcastIntent.putExtra("Bootloader", ioio.getImplVersion(IOIO.VersionType.BOOTLOADER_VER));
        broadcastIntent.putExtra("Hardware", ioio.getImplVersion(IOIO.VersionType.HARDWARE_VER));
        sendBroadcast(broadcastIntent);

        String g = ioio.getImplVersion(IOIO.VersionType.IOIOLIB_VER);
        String g1 = ioio.getImplVersion(IOIO.VersionType.APP_FIRMWARE_VER);
        String g2 = ioio.getImplVersion(IOIO.VersionType.BOOTLOADER_VER);
        String g3 = ioio.getImplVersion(IOIO.VersionType.HARDWARE_VER);
    }

    private void showDisconnect()
    {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("ioio_disconnect");
        sendBroadcast(broadcastIntent);
    }

    private void showLoop()
    {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("ioio_loop");
        sendBroadcast(broadcastIntent);
    }


    public void SetData(String who, int round, Boolean checkround, Boolean checkpause, Boolean checkstart,
                        String route_name, String start_name, String target_name, Double start_lat,
                        Double start_lng, Double target_lat, Double target_lng, Double route_distance) {

        this.round = round;
        this.checkround = checkround;
        this.checkpause = checkpause;
        this.checkstart = checkstart;

        this.route_name = route_name;
        this.start_name = start_name;
        this.target_name = target_name;
        this.start_lat = start_lat;
        this.start_lng = start_lng;
        this.target_lat = target_lat;
        this.target_lng = target_lng;
        this.route_distance = route_distance;


        Log.e("SetData "," this.checkstart = "+this.checkstart+"\n"
                        +" mGoogleApiClient = "+mGoogleApiClient+"\n");
        if(who.equals("start") || who.equals("stop"))
        {
            if(this.checkstart)
            {

                if (mGoogleApiClient.isConnected()) {

                    State = 0;
                    startLocationUpdates();
                }
                else
                {
                    mGoogleApiClient.connect();

                    if (mGoogleApiClient.isConnected()) {
                        State = 0;
                        startLocationUpdates();
                    }
                }


            }
            else
            {
                stopLocationUpdates();

            }
        }
        else if(who.equals("pause"))
        {
            Log.e(" Set data"," : "+who);
            if(this.checkpause)
            {

                if (mGoogleApiClient.isConnected()) {

                    startLocationUpdates();
                }
                else
                {
                    mGoogleApiClient.connect();

                    if (mGoogleApiClient.isConnected()) {

                        startLocationUpdates();
                    }
                }


            }
            else
            {
                stopLocationUpdates();

            }
        }


    }

    public void CallStop() {

        if(last_location != null)
        {
            State = 5;
            StopTrackSave(last_location);
        }

    }

    public class MyBinder extends Binder {
        BoundService getService() {
            return BoundService.this;
        }
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.i(" Service ", "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Resuming the periodic location updates
        if (mGoogleApiClient.isConnected()) {
            //startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        // Assign the new location
        mLastLocation = location;

        if(mLastLocation != null)
        {

            last_location = location;

                if(State == 0)
                {
                    if(Distance.calculateDistance(last_location.getLatitude(), last_location.getLongitude()
                            , start_lat, start_lng) < 100.0)
                    {
                        check_save(location);
                    }
                    else
                    {
                        State = 1;
                        check_save(location);
                    }


                }
                else if(State == 1)
                {
                    if(Distance.calculateDistance(last_location.getLatitude(), last_location.getLongitude()
                            , target_lat, target_lng) > 100.0)
                    {
                        check_save(location);
                    }
                    else
                    {
                        State = 2;
                        check_save(location);
                    }


                }
                else if(State == 2)
                {
                    if(Distance.calculateDistance(last_location.getLatitude(), last_location.getLongitude()
                            , target_lat, target_lng) < 100.0)
                    {
                        check_save(location);
                    }
                    else
                    {
                        State = 3;
                        check_save(location);
                    }


                }
                else if(State == 3)
                {
                    if(Distance.calculateDistance(last_location.getLatitude(), last_location.getLongitude()
                            , start_lat, start_lng) > 100.0)
                    {
                        check_save(location);
                    }
                    else
                    {
                        State = 4;
                        check_save(location);
                    }


                }
                else if(State == 4)
                {
                    if(Distance.calculateDistance(last_location.getLatitude(), last_location.getLongitude()
                            , target_lat, target_lng) < 100.0)
                    {
                        check_save(location);
                    }
                    else
                    {
                        State = 5;
                        check_save(location);
                    }


                }





        }

    }


    private void Initial() {

        State = 0;
        round = 0;
        checkround = false;
        checkpause = false;
        checkstart = false;

        SharedPreferences prefs = new SecurePreferences(getApplicationContext(), "pass", "secure");

        ConfigData.api_key = prefs.getString("api_key", "");

    }

    private void check_save(Location location)
    {
        Notify("GPS Tracking ",(last_location.getSpeed()* 3.6)+" Km/h");

        //Log.e(" onLocationChanged ","round : "+round+" checkpause : "+checkpause+" checkround : "+ checkround+" checkstart : "+ checkstart);

        if(checkpause && checkround && checkstart)
        {

            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("velocity");
            broadcastIntent.putExtra("velocity", last_location.getSpeed());
            sendBroadcast(broadcastIntent);
            Log.e(" onLocationChanged "," getLatitude : "+last_location.getLatitude() + " getLongitude : "+last_location.getLongitude());

            TrackSave(location);
        }
    }



    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Creating location request object
     * */
    protected void createLocationRequest() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(DISPLACEMENT);

    }




    /**
     * Starting the location updates
     * */
    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    private void TrackSave(Location location)
    {

        TrackSave_API trackSave_api = ApiClient.getClient().create(TrackSave_API.class);

        Call<TrackSave_Model> call = trackSave_api.TrackSave(ConfigData.api_key, location.getLatitude()+""
                , location.getLongitude()+"", round+"", location.getSpeed()+"", mAzimuth, "run", State);
        call.enqueue(new Callback<TrackSave_Model>() {
            @Override
            public void onResponse(Call<TrackSave_Model> call, Response<TrackSave_Model> response) {

                if(!response.body().getError())
                {
                    Log.e(" TrackSave "," onResponse "+response.body().getMessage());
                }
                else
                {
                    Log.e(" TrackSave "," onResponse "+response.body().getMessage());
                }

            }

            @Override
            public void onFailure(Call<TrackSave_Model>call, Throwable t) {
                // Log error here since request failed
                Log.e(" TrackSave  onFailure ", t.toString());
            }
        });
    }

    private void StopTrackSave(Location location)
    {
        TrackSave_API trackSave_api = ApiClient.getClient().create(TrackSave_API.class);

        Call<TrackSave_Model> call = trackSave_api.TrackSave(ConfigData.api_key, location.getLatitude()+""
                , location.getLongitude()+"", round+"", location.getSpeed()+"", mAzimuth, "stop", State);
        call.enqueue(new Callback<TrackSave_Model>() {
            @Override
            public void onResponse(Call<TrackSave_Model> call, Response<TrackSave_Model> response) {

                if(!response.body().getError())
                {
                    stopForeground(true);
                    stopSelf();
                    Log.e(" StopTrackSave "," onResponse "+response.body().getMessage());
                }
                else
                {
                    Log.e(" StopTrackSave "," onResponse "+response.body().getMessage());
                }

            }

            @Override
            public void onFailure(Call<TrackSave_Model>call, Throwable t) {
                // Log error here since request failed
                Log.e(" TrackSave  onFailure ", t.toString());
            }
        });
    }


    private SensorEventListener mSensorEventListener = new SensorEventListener() {

        float[] gData = new float[3]; // accelerometer
        float[] mData = new float[3]; // magnetometer
        float[] rMat = new float[9];
        float[] iMat = new float[9];
        float[] orientation = new float[3];

        public void onAccuracyChanged( Sensor sensor, int accuracy ) {}

        @Override
        public void onSensorChanged( SensorEvent event ) {
            float[] data;
            switch ( event.sensor.getType() ) {
                case Sensor.TYPE_ACCELEROMETER:
                    gData = event.values.clone();
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    mData = event.values.clone();
                    break;
                default: return;
            }

            if ( SensorManager.getRotationMatrix( rMat, iMat, gData, mData ) ) {
                mAzimuth= (int) ( Math.toDegrees( SensorManager.getOrientation( rMat, orientation )[0] ) + 360 ) % 360;

                // Log.e(" Main "," mAzimuth : "+mAzimuth);
            }
        }
    };

    private void Notify(String notificationTitle, String notificationMessage){

        PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setTicker(notificationTitle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(notificationTitle)
                .setContentText(notificationMessage)
                .setContentIntent(pi)
                .setAutoCancel(false)
                .setOngoing(true)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        //notificationManager.notify(0, notification);
        startForeground(2, notification);
    }

}