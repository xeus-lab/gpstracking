package xyz.stepsecret.gpstracking;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mukesh.tinydb.TinyDB;
import com.securepreferences.SecurePreferences;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import xyz.stepsecret.gpstracking.API.Login_API;
import xyz.stepsecret.gpstracking.Config.ApiClient;
import xyz.stepsecret.gpstracking.Config.ConfigData;
import xyz.stepsecret.gpstracking.Model.Login_Model;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private TinyDB Store_data;

    EditText input_username;
    EditText input_password;
    Button btn_login;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        input_username = (EditText) findViewById(R.id.input_username);
        input_password = (EditText) findViewById(R.id.input_password);
        btn_login = (Button) findViewById(R.id.btn_login);

        Store_data = new TinyDB(getApplicationContext());

        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });


        Check_login();

    }



    public void Check_login()
    {
        Boolean login = Store_data.getBoolean("login");
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        String macAddress = wInfo.getMacAddress();

        Log.e(" StartUp "," macAddress : "+macAddress);


        if(macAddress.equals(getResources().getString(R.string.mac)))
        {
            Log.e(" Start Up "," login : "+login);
            if(login)
            {

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }


        }
        else
        {
            finish();
        }

    }

    public void login() {
        Log.d(TAG, "Login");

        String username = input_username.getText().toString();
        String password = input_password.getText().toString();

        Call_login(username,password);
    }

    public void Call_login(String username,String password)
    {
        Login_API login_api = ApiClient.getClient().create(Login_API.class);

        Call<Login_Model> call = login_api.LoginAPI(username, password);
        call.enqueue(new Callback<Login_Model>() {
            @Override
            public void onResponse(Call<Login_Model> call, Response<Login_Model> response) {

                if(!response.body().getError())
                {
                    Log.e(" Login "," >> "+response.body().getError());
                    Log.e(" Login "," >> "+response.body().getFirst_name());
                    Log.e(" Login "," api_key >> "+response.body().getApiKey());

                    SharedPreferences prefs = new SecurePreferences(getApplicationContext(), "pass" , "secure");
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("api_key", response.body().getApiKey());
                    editor.commit();

                    Store_data.putBoolean("login", true);
                    Store_data.putString("first_name", response.body().getFirst_name());
                    Store_data.putString("last_name", response.body().getLast_name());

                    Check_login();
                }




            }

            @Override
            public void onFailure(Call<Login_Model>call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    public void show_failure(String message)
    {
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(message)
                .show();
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }






}
