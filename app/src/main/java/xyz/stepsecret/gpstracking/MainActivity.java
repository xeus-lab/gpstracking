package xyz.stepsecret.gpstracking;


import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mukesh.tinydb.TinyDB;
import com.securepreferences.SecurePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.stepsecret.gpstracking.API.Connect_API;
import xyz.stepsecret.gpstracking.API.Round_API;
import xyz.stepsecret.gpstracking.Config.ApiClient;
import xyz.stepsecret.gpstracking.Config.ConfigData;
import xyz.stepsecret.gpstracking.Model.Connect_Model;
import xyz.stepsecret.gpstracking.Model.Round_Model;

public class MainActivity extends AppCompatActivity {

    private Button start_location;
    private Button stop_location;

    private Button pause_location;

    private Boolean beep = false;


    private TextView tv_status, tv_IOIOLib, tv_Application_firmware, tv_Bootloader_firmware, tv_Hardware, tv_sp, tv_status_connect;

    private CardView cv_ioio;

    private TinyDB Store_data;

    private Boolean checkpause;
    private Boolean checkround;
    private Boolean checkstart;
    private Boolean checkconnect;
    private int round;

    private BoundService mBoundService;
    private boolean mServiceBound = false;

    private IntentFilter mIntentFilter;

    private Spinner sp_route;

    private String route_name[];
    private ArrayAdapter<String> adapterroute_name;

    private ArrayList<String> temp_id_route = new ArrayList<>();
    private ArrayList<String> temp_route_route_name = new ArrayList<>();
    private ArrayList<String> temp_route_start_name = new ArrayList<>();
    private ArrayList<String> temp_route_target_name = new ArrayList<>();
    private ArrayList<String> temp_route_start_lat = new ArrayList<>();
    private ArrayList<String> temp_route_start_lng = new ArrayList<>();
    private ArrayList<String> temp_route_target_lat = new ArrayList<>();
    private ArrayList<String> temp_route_target_lng = new ArrayList<>();
    private ArrayList<String> temp_route_route_distance = new ArrayList<>();

    private int position_route = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        start_location = (Button) findViewById(R.id.start_location);
        stop_location = (Button) findViewById(R.id.stop_location);
        pause_location = (Button) findViewById(R.id.pause_location);


        tv_status = (TextView) findViewById(R.id.tv_status);
        tv_IOIOLib = (TextView) findViewById(R.id.tv_IOIOLib);
        tv_Application_firmware = (TextView) findViewById(R.id.tv_Application_firmware);
        tv_Bootloader_firmware = (TextView) findViewById(R.id.tv_Bootloader_firmware);
        tv_Hardware = (TextView) findViewById(R.id.tv_Hardware);

        tv_sp = (TextView) findViewById(R.id.tv_sp);

        tv_status_connect = (TextView) findViewById(R.id.tv_status_connect);


        cv_ioio = (CardView) findViewById(R.id.cv_ioio);

        cv_ioio.setVisibility(View.GONE);

        start_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Location permission not granted
                checkstart = true;
                Store_data.putBoolean("checkstart",true);
                GetRound();

            }
        });

        stop_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Location permission not granted

                if(checkstart)
                {
                    pause_location.setVisibility(View.GONE);
                    start_location.setVisibility(View.VISIBLE);

                    checkround = false;
                    checkpause = false;
                    checkstart = false;
                    pause_location.setText("Pause");

                    Store_data.putBoolean("checkround",false);
                    Store_data.putBoolean("checkpause",false);
                    Store_data.putBoolean("checkstart",false);

                    mBoundService.SetData("stop", round, checkround, checkpause, checkstart,
                            temp_route_route_name.get(position_route),
                            temp_route_start_name.get(position_route),
                            temp_route_target_name.get(position_route),
                            Double.parseDouble(temp_route_start_lat.get(position_route)),
                            Double.parseDouble(temp_route_start_lng.get(position_route)),
                            Double.parseDouble(temp_route_target_lat.get(position_route)),
                            Double.parseDouble(temp_route_target_lng.get(position_route)),
                            Double.parseDouble(temp_route_route_distance.get(position_route)));

                    mBoundService.CallStop();
                }


            }
        });

        pause_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkpause)
                {
                    checkpause = false;
                    Store_data.putBoolean("checkpause",false);
                    pause_location.setText("Resume");

                    mBoundService.SetData("pause", round, checkround, checkpause, checkstart,
                            temp_route_route_name.get(position_route),
                            temp_route_start_name.get(position_route),
                            temp_route_target_name.get(position_route),
                            Double.parseDouble(temp_route_start_lat.get(position_route)),
                            Double.parseDouble(temp_route_start_lng.get(position_route)),
                            Double.parseDouble(temp_route_target_lat.get(position_route)),
                            Double.parseDouble(temp_route_target_lng.get(position_route)),
                            Double.parseDouble(temp_route_route_distance.get(position_route)));


                    Log.e(" MAIN "," pause :>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+checkpause);


                }
                else
                {
                    checkpause = true;
                    Store_data.putBoolean("checkpause",true);
                    pause_location.setText("Pause");

                    mBoundService.SetData("pause", round, checkround, checkpause, checkstart,
                            temp_route_route_name.get(position_route),
                            temp_route_start_name.get(position_route),
                            temp_route_target_name.get(position_route),
                            Double.parseDouble(temp_route_start_lat.get(position_route)),
                            Double.parseDouble(temp_route_start_lng.get(position_route)),
                            Double.parseDouble(temp_route_target_lat.get(position_route)),
                            Double.parseDouble(temp_route_target_lng.get(position_route)),
                            Double.parseDouble(temp_route_route_distance.get(position_route)));

                    Log.e(" MAIN "," pause :>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+checkpause);
                }


            }
        });

        sp_route = (Spinner) findViewById(R.id.input_route);

        sp_route.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,int position, long id) {

                position_route = position;

                Log.e(" Main "," :> "+adapter.getItemAtPosition(position).toString());



            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        isGooglePlayServicesAvailable(getApplicationContext());

        InitialSpinner();

        InitialAPIKey();


    }

    private void InitialSpinner()
    {
        Store_data = new TinyDB(getApplicationContext());

        temp_id_route = Store_data.getListString("id_route");
        temp_route_route_name = Store_data.getListString("route_name");
        temp_route_start_name = Store_data.getListString("start_name");
        temp_route_target_name = Store_data.getListString("target_name");
        temp_route_start_lat = Store_data.getListString("start_lat");
        temp_route_start_lng = Store_data.getListString("start_lng");
        temp_route_target_lat = Store_data.getListString("target_lat");
        temp_route_target_lng = Store_data.getListString("target_lng");
        temp_route_route_distance = Store_data.getListString("route_distance");

       // route_name = temp_route_route_name.

            route_name = new String[temp_route_route_name.size()];
            for(int i = 0 ; i < temp_route_route_name.size(); i++)
            {
                route_name[i] = temp_route_route_name.get(i);
            }


        adapterroute_name = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, route_name);

        adapterroute_name.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_route.setAdapter(adapterroute_name);


    }

    private void InitialAPIKey() {

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("velocity");
        mIntentFilter.addAction("ioio_connect");
        mIntentFilter.addAction("ioio_disconnect");
        mIntentFilter.addAction("ioio_loop");

        round = Store_data.getInt("round");
        checkround = Store_data.getBoolean("checkround");
        checkpause = Store_data.getBoolean("checkpause");
        checkstart = Store_data.getBoolean("checkstart");
        checkconnect = false;
        Log.e(" Main "," round : "+round);
        Log.e(" Main "," checkround : "+checkround);
        Log.e(" Main "," checkpause : "+checkpause);
        Log.e(" Main "," checkstart : "+checkstart);

        if (checkstart)
        {
            start_location.setVisibility(View.GONE);
            pause_location.setVisibility(View.VISIBLE);

            if(checkpause)
            {
                pause_location.setText("Pause");
            }
            else
            {
                pause_location.setText("Resume");
            }
        }
        else
        {
            start_location.setVisibility(View.VISIBLE);
            pause_location.setVisibility(View.GONE);
        }

        SharedPreferences prefs = new SecurePreferences(getApplicationContext(), "pass", "secure");

        ConfigData.api_key = prefs.getString("api_key", "");


    }

    @Override
    protected void onStart() {
        super.onStart();


        CheckConnect();
        Intent intent = new Intent(this, BoundService.class);
        startService(intent);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);


    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(mReceiver, mIntentFilter);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mServiceBound) {
            unbindService(mServiceConnection);
            mServiceBound = false;
        }

    }



    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);

    }
    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {

            mServiceBound = false;
            Store_data.putBoolean("mServiceBound",mServiceBound);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BoundService.MyBinder myBinder = (BoundService.MyBinder) service;
            mBoundService = myBinder.getService();
            mServiceBound = true;
            Store_data.putBoolean("mServiceBound",mServiceBound);
        }
    };

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("velocity")) {
                tv_sp.setText((intent.getFloatExtra("velocity",0) * 3.6)+" Km/h");

                Log.e(" Main "," velocity : "+intent.getFloatExtra("velocity",0));
            }
            else if(intent.getAction().equals("ioio_connect"))
            {
                cv_ioio.setVisibility(View.VISIBLE);

                tv_status.setText("Status : " + intent.getStringExtra("Status"));
                tv_IOIOLib.setText("IOIOLib : " + intent.getStringExtra("IOIOLib"));
                tv_Application_firmware.setText("Application firmware : " + intent.getStringExtra("Application"));
                tv_Bootloader_firmware.setText("Bootloader firmware : " + intent.getStringExtra("Bootloader"));
                tv_Hardware.setText("Hardware : " + intent.getStringExtra("Hardware"));
            }
            else if(intent.getAction().equals("ioio_disconnect"))
            {
                cv_ioio.setVisibility(View.GONE);

                tv_status.setText("Status : ");
                tv_IOIOLib.setText("IOIOLib : ");
                tv_Application_firmware.setText("Application firmware : ");
                tv_Bootloader_firmware.setText("Bootloader firmware : ");
                tv_Hardware.setText("Hardware : ");
            }

           // Log.e("BroadcastReceiver ","intent.getAction() : "+intent.getAction());
        }
    };

    /**
     * Method to verify google play services on the device
     * */
    public boolean isGooglePlayServicesAvailable(Context context){
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(context);


        if(resultCode == ConnectionResult.SUCCESS)
        {
            Log.e(" Main ",">>>>>>>>>> : is GooglePlayServicesAvailable");
            return true;
        }
        else
        {
            Log.e(" Main ",">>>>>>>>>> : is not GooglePlayServicesAvailable");

            final String appPackageName = "com.google.android.gms";
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
            }

            finish();
            return false;
        }
    }


    private void GetRound()
    {
        if(checkconnect)
        {
            checkround = false;

            Round_API round_api = ApiClient.getClient().create(Round_API.class);

            Call<Round_Model> call = round_api.gerRound(ConfigData.api_key);
            call.enqueue(new Callback<Round_Model>() {
                @Override
                public void onResponse(Call<Round_Model> call, Response<Round_Model> response) {

                    if(!response.body().getError())
                    {

                        round = response.body().getRound();
                        checkround = true;
                        checkpause = true;
                        Store_data.putInt("round",round);
                        Store_data.putBoolean("checkround",true);
                        Store_data.putBoolean("checkpause",true);
                        pause_location.setVisibility(View.VISIBLE);
                        start_location.setVisibility(View.GONE);

                        mBoundService.SetData("start", round, checkround, checkpause, checkstart,
                                temp_route_route_name.get(position_route),
                                temp_route_start_name.get(position_route),
                                temp_route_target_name.get(position_route),
                                Double.parseDouble(temp_route_start_lat.get(position_route)),
                                Double.parseDouble(temp_route_start_lng.get(position_route)),
                                Double.parseDouble(temp_route_target_lat.get(position_route)),
                                Double.parseDouble(temp_route_target_lng.get(position_route)),
                                Double.parseDouble(temp_route_route_distance.get(position_route)));

                        Log.e(" GetRound  onResponse ", " success "+response.body().getRound());

                    }

                }

                @Override
                public void onFailure(Call<Round_Model>call, Throwable t) {
                    // Log error here since request failed
                    Log.e(" GetRound  onFailure ", t.toString());
                }
            });
        }
        else
        {
            CheckConnect();
        }

    }


    private void CheckConnect()
    {
        Connect_API connect_api = ApiClient.getClient().create(Connect_API.class);

        Call<Connect_Model> call = connect_api.gerCheckConnect();
        call.enqueue(new Callback<Connect_Model>() {
            @Override
            public void onResponse(Call<Connect_Model> call, Response<Connect_Model> response) {

                if(!response.body().getError())
                {
                    tv_status_connect.setText("Status Connect : Is Connecting...");
                    checkconnect = true;
                }
                else
                {
                    Log.e(" GetRound  onFailure ", "");
                }

            }

            @Override
            public void onFailure(Call<Connect_Model>call, Throwable t) {
                // Log error here since request failed
                Log.e(" GetRound  onFailure ", t.toString());
                tv_status_connect.setText("Status Connect : Is Failure...");
                checkconnect = false;
            }
        });
    }


}