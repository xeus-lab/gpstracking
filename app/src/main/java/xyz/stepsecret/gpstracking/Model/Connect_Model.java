package xyz.stepsecret.gpstracking.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Assanee on 8/7/2558.
 */
public class Connect_Model {

    @SerializedName("error")
    private Boolean error ;


    @SerializedName("message")
    private String message;


    public Boolean getError() {
        return error;
    }

    public String getMessage() {
            return message;
        }


}
