package xyz.stepsecret.gpstracking.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Assanee on 8/7/2558.
 */
public class Round_Model {

    @SerializedName("error")
    private Boolean error ;


    @SerializedName("round")
    private int round;


    public Boolean getError() {
        return error;
    }

    public int getRound() {
        return round;
    }


}
