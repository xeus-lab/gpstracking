package xyz.stepsecret.gpstracking.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Assanee on 8/7/2558.
 */
public class Route_Model {

    @SerializedName("error")
    private Boolean error ;

    @SerializedName("data")
    private String[][] data;

    @SerializedName("message")
    private String message;

    public Boolean getError() {
        return error;
    }

    public String[][] getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }


}
