package xyz.stepsecret.gpstracking;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mukesh.tinydb.TinyDB;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.stepsecret.gpstracking.API.Route_API;
import xyz.stepsecret.gpstracking.Config.ApiClient;
import xyz.stepsecret.gpstracking.Model.Route_Model;

/**
 * Created by stepsecret on 14/9/2559.
 */
public class StartUp extends AppCompatActivity {

    private SweetAlertDialog pDialog;

    private TinyDB Store_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            pDialog();

            Initial_();

            showGPSDisabledAlertToUser();

    }
    private void showGPSDisabledAlertToUser(){

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {



                    WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                    WifiInfo wInfo = wifiManager.getConnectionInfo();
                    String macAddress = wInfo.getMacAddress();

                    Log.e(" StartUp "," macAddress : "+macAddress);

                    if(macAddress.equals(getResources().getString(R.string.mac)))
                    {
                        Get_Route();
                    }
                    else
                    {
                        finish();
                    }




        }
        else
        {

            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("Turn On GPS");
            dialog.setIcon(R.mipmap.ic_launcher);
            dialog.setCancelable(true);
            dialog.setMessage("Please!!! Turn On GPS");
            dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 1);
                }
            });

            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    new CountDownTimer(5000, 1000){
                        public void onTick(long millisUntilDone){

                            Log.e(" StartUp "," "+millisUntilDone);
                        }

                        public void onFinish() {

                            showGPSDisabledAlertToUser();

                        }
                    }.start();

                }
            });

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(final DialogInterface dialog) {

                    Log.e(" dialog "," setOnDismissListener");


                }
            });
            dialog.show();


        }


    }


    private void pDialog()
    {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();
    }


    private void Initial_()
    {
        Store_data = new TinyDB(getApplicationContext());

        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                //Toast.makeText(LoginActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                //Toast.makeText(LoginActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }


        };


        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setRationaleMessage("we need permission for find your location")
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setGotoSettingButtonText("Go to setting")
                .setPermissions(
                        android.Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION)
                .check();


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        pDialog.dismiss();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        new CountDownTimer(5000, 1000){
            public void onTick(long millisUntilDone){

                Log.e(" StartUp "," "+millisUntilDone);
            }

            public void onFinish() {

                showGPSDisabledAlertToUser();

            }
        }.start();


    }

    private void Get_Route()
    {
        Route_API route_api = ApiClient.getClient().create(Route_API.class);

        Call<Route_Model> call = route_api.gerRoute();
        call.enqueue(new Callback<Route_Model>() {
            @Override
            public void onResponse(Call<Route_Model> call, Response<Route_Model> response) {

                if(!response.body().getError())
                {
                    pDialog.dismiss();

                   // Store_data.remove("route");

                    ArrayList<String> temp_id_route = new ArrayList<>();
                    ArrayList<String> temp_route_route_name = new ArrayList<>();
                    ArrayList<String> temp_route_start_name = new ArrayList<>();
                    ArrayList<String> temp_route_target_name = new ArrayList<>();
                    ArrayList<String> temp_route_start_lat = new ArrayList<>();
                    ArrayList<String> temp_route_start_lng = new ArrayList<>();
                    ArrayList<String> temp_route_target_lat = new ArrayList<>();
                    ArrayList<String> temp_route_target_lng = new ArrayList<>();
                    ArrayList<String> temp_route_route_distance = new ArrayList<>();

                    for(int i = 0 ; i < response.body().getData().length ; i++)
                    {

                        temp_id_route.add(response.body().getData()[i][0]);
                        temp_route_route_name.add(response.body().getData()[i][1]);
                        temp_route_start_name.add(response.body().getData()[i][2]);
                        temp_route_target_name.add(response.body().getData()[i][3]);
                        temp_route_start_lat.add(response.body().getData()[i][4]);
                        temp_route_start_lng.add(response.body().getData()[i][5]);
                        temp_route_target_lat.add(response.body().getData()[i][6]);
                        temp_route_target_lng.add(response.body().getData()[i][7]);
                        temp_route_route_distance.add(response.body().getData()[i][8]);

                    }

                    Store_data.putString("route", new Gson().toJson(response.body().getData()));

                    Store_data.putListString("id_route", temp_id_route);
                    Store_data.putListString("route_name", temp_route_route_name);
                    Store_data.putListString("start_name", temp_route_start_name);
                    Store_data.putListString("target_name", temp_route_target_name);
                    Store_data.putListString("start_lat", temp_route_start_lat);
                    Store_data.putListString("start_lng", temp_route_start_lng);
                    Store_data.putListString("target_lat", temp_route_target_lat);
                    Store_data.putListString("target_lng", temp_route_target_lng);
                    Store_data.putListString("route_distance", temp_route_route_distance);

                    Boolean login = Store_data.getBoolean("login");

                        Log.e(" Start Up "," login : "+login);
                        if(login)
                        {

                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        }
                        else
                        {
                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        }



                }
                else
                {
                    Get_Route();
                    Log.e(" GetRoute  onFailure ", "");
                }

            }

            @Override
            public void onFailure(Call<Route_Model>call, Throwable t) {
                // Log error here since request failed
                Get_Route();
                Log.e(" GetRoute  onFailure ", t.toString());

            }
        });
    }



}
